<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
                        
class Admin_login
{
                        
    protected $CI;

    public function __construct()
    {
                                    
        $this->CI =& get_instance();
        // load model user
        $this->CI->load->model('user_model');
    }

    // Fungsi login
    public function login($username, $password){
        $check = $this->CI->user_model->login($username, $password);
        
        // Jika ada data user, Maka create session login
        if ($check) {
            $id_user    = $check->id_user;
            $nama_user  = $check->nama_user;
            $jabatan    = $check->jabatan;
            $gambar     = $check->gambar;

            // Create session
            $this->CI->session->set_userdata('id_user', $id_user);
            $this->CI->session->set_userdata('nama_user', $nama_user);
            $this->CI->session->set_userdata('jabatan', $jabatan);
            $this->CI->session->set_userdata('gambar', $gambar);

            // Redirect ke halaman dashbaord
            redirect('admin/dashboard','refresh');
            
        }else {
            // kalau tidak ada(Username & password salah),maka suruh login lagi
            $this->CI->session->set_flashdata('warning', '<strong>Username atau Password salah !!</strong>');
            redirect(base_url('root'), 'refresh');
        }
    }

    // Fungsi cek login
    public function cek_login(){
        // mememeriksa apakah sudah login
        if ($this->CI->session->userdata('username') == " ") {
            $this->CI->session->set_flashdata('warning', '<strong>Anda belum login....</strong>');
            redirect(base_url('root'), 'refresh');
        }
    }

    // Fungsi log out
    public function logout(){
        // Membuang semua session
        $this->CI->session->unset_userdata('id_user');
        $this->CI->session->unset_userdata('nama_user');
        $this->CI->session->unset_userdata('username');
        $this->CI->session->unset_userdata('jabatan');
        $this->CI->session->unset_userdata('gambar');

        // Setelah session dibuang,redirect ke login
        $this->CI->session->set_flashdata('sukses', '<strong>Anda berhasil logout....</strong>');
        redirect(base_url('root'), 'refresh');
    }
                        
                            
}
                                                
/* End of file Admin_login.php */
    
                        