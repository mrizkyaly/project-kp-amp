<?php 

defined('BASEPATH') OR exit('No direct script access allowed');
                        
class Merk_model extends CI_Model {
                        
    
    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }
    
    public function listing(){
        $this->db->select('*');
        $this->db->from('merk');
        $this->db->order_by('id_merk', 'desc');
        $query = $this->db->get();
        return $query->result();
    }

    public function detail($id_merk){
        $this->db->select('*');
        $this->db->from('merk');
        $this->db->where('id_merk', $id_merk);
        $this->db->order_by('id_merk', 'desc');
        $query = $this->db->get();
        return $query->row();   
    }

    public function total_merk(){
        $this->db->select('COUNT(*) AS total');
        $this->db->from('merk');
        $query = $this->db->get();
        return $query->row();
    }

    public function tambah($data){
        $this->db->insert('merk', $data);
    }

    public function edit($data){
        $this->db->where('id_merk', $data['id_merk']);
        $this->db->update('merk', $data);      
    }

    public function delete($data){
        $this->db->where('id_merk', $data['id_merk']);
        $this->db->delete('merk', $data);
    }
                        
                            
                        
}
                        
/* End of file Merk.php */
    
                        