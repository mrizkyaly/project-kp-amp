<?php 

defined('BASEPATH') OR exit('No direct script access allowed');
                        
class Produk_model extends CI_Model {
                        
    
    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }
    
    public function listing(){
        $this->db->select(' produk.*,
                            users.nama_user,
                            jenis.nama_jenis,
                            merk.nama_merk,
                            ekspedisi.kota_tujuan,
                            ekspedisi.harga_ekspedisi'
                        );
        $this->db->from('produk');
        $this->db->join('users', 'users.id_user = produk.id_user', 'left');
        $this->db->join('jenis', 'jenis.id_jenis = produk.id_jenis', 'left');
        $this->db->join('merk', 'merk.id_merk = produk.id_merk', 'left');
        $this->db->join('ekspedisi', 'ekspedisi.id_ekspedisi = produk.id_ekspedisi', 'left');
        $this->db->order_by('id_produk', 'desc');
        $query = $this->db->get();
        return $query->result();
    }

    public function detail_produk($slug_produk){
        $this->db->select(' produk.*,
                            users.nama_user,
                            jenis.nama_jenis,
                            merk.nama_merk,
                            ekspedisi.kota_tujuan,
                            ekspedisi.harga_ekspedisi' 
                        );
        $this->db->from('produk');
        $this->db->join('users', 'users.id_user = produk.id_user', 'left');
        $this->db->join('jenis', 'jenis.id_jenis = produk.id_jenis', 'left');
        $this->db->join('merk', 'merk.id_merk = produk.id_merk', 'left');
        $this->db->join('ekspedisi', 'ekspedisi.id_ekspedisi = produk.id_ekspedisi', 'left');
        $this->db->where('produk.slug_produk', $slug_produk);
        $this->db->order_by('id_produk', 'desc');
        $query = $this->db->get();
        return $query->row();
    }

    public function detail($id_produk){
        $this->db->select('*');
        $this->db->from('produk');
        $this->db->where('id_produk', $id_produk);
        $this->db->order_by('id_produk', 'desc');
        $query = $this->db->get();
        return $query->row();
    }

    public function total_produk(){
        $this->db->select('COUNT(*) AS total');
        $this->db->from('produk');
        $query = $this->db->get();
        return $query->row();
    }

    public function tambah($data){
        $this->db->insert('produk', $data);
    }

    public function edit($data){
        $this->db->where('id_produk', $data['id_produk']);
        $this->db->update('produk', $data);
    }

    public function delete($data){
        $this->db->where('id_produk', $data['id_produk']);
        $this->db->delete('produk',$data);
    }
                        
                            
                        
}
                        
/* End of file Produk.php */
    
                        