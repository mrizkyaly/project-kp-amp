<?php 

defined('BASEPATH') OR exit('No direct script access allowed');
                        
class Transaksi_model extends CI_Model {
                        
    
    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }
    
    public function listing()
    {
        $this->db->select(' transaksi.*,
                            produk.gambar,
                            produk.nama_produk,
                            produk.harga_pokok,
                            produk.harga_jual,
                            satuan.nama_satuan'
                        );
        $this->db->from('transaksi');
        $this->db->join('produk', 'produk.id_produk = transaksi.id_produk', 'left');
        $this->db->join('satuan', 'satuan.id_satuan = transaksi.id_satuan', 'left');
        $this->db->order_by('id_transaksi', 'desc');
        $query = $this->db->get();
        return $query->result();
    }

    public function detail($id_transaksi){
        $this->db->select('*');
        $this->db->from('transaksi');
        $this->db->where('id_transaksi', $id_transaksi);
        $this->db->order_by('id_transaksi', 'desc');
        $query = $this->db->get();
        return $query->row();
    }

    public function total_transaksi(){
        $this->db->select('COUNT(*) AS total');
        $this->db->from('transaksi');
        $query = $this->db->get();
        return $query->row();
    }

    public function tambah($data){
        $this->db->insert('transaksi', $data);
    }

    public function edit($data){
        $this->db->where('id_transaksi', $data['id_transaksi']);
        $this->db->update('transaksi', $data);
    }

    public function delete($data){
        $this->db->where('id_transaksi', $data['id_transaksi']);
        $this->db->delete('transaksi',$data);
    }

    public function export()
    {
        $this->db->select('*');
        $this->db->from('transaksi');
        return $this->db->get();
    }
                        
                            
                        
}
                        
/* End of file Transaksi.php */
    
                        