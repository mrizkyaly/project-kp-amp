<?php 

defined('BASEPATH') OR exit('No direct script access allowed');
                        
class Ekspedisi_model extends CI_Model {
                        
    
    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }
    
    // View all data ekspedisi
    public function listing(){
        $this->db->select('*');
        $this->db->from('ekspedisi');
        $this->db->order_by('id_ekspedisi', 'desc');
        $query = $this->db->get();
        return $query->result();
    }
    
    public function detail($id_ekspedisi){
        $this->db->select('*');
        $this->db->from('ekspedisi');
        $this->db->where('id_ekspedisi', $id_ekspedisi);
        $this->db->order_by('id_ekspedisi', 'desc');
        $query = $this->db->get();
        return $query->row();
    }

    public function total_ekspedisi(){
        $this->db->select('COUNT(*) AS total');
        $this->db->from('ekspedisi');
        $query = $this->db->get();
        return $query->row();
    }

    public function tambah($data){
        $this->db->insert('ekspedisi', $data);
        
    }

    public function edit($data){
        $this->db->where('id_ekspedisi', $data['id_ekspedisi']);
        $this->db->update('ekspedisi', $data);
    }

    public function delete($data){
        $this->db->where('id_ekspedisi', $data['id_ekspedisi']);
        $this->db->delete('ekspedisi', $data);
    }
                        
                            
                        
}
                        
/* End of file ekspedisi.php */
    
                        