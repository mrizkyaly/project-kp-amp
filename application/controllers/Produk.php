<?php 
        
defined('BASEPATH') OR exit('No direct script access allowed');
        
class Produk extends CI_Controller {
    
    public function __construct()
    {
        parent::__construct();
        $this->load->model('transaksi_model');
        $this->load->model('produk_model');
        $this->load->model('satuan_model');
    }

    public function index(){
        $produk = $this->produk_model->listing();

        $data =  array( 'title'     => 'Produk',
                        'produk'    => $produk,
                        'isi'       => 'produk/list'
                    );
        $this->load->view('layout/wrapper', $data, FALSE);
        
    }
    

    public function detail($slug_produk){
        $produk = $this->produk_model->detail_produk($slug_produk);

        $data = array(  'title' => $produk->nama_produk,
                        'produk' => $produk,
                        'isi' => 'produk/detail'
                    );
        $this->load->view('layout/wrapper', $data, FALSE);
    }

    public function transaksi($id_produk){
        $produk = $this->produk_model->detail($id_produk);
        $produk_detail = $this->produk_model->detail($id_produk);
        $satuan = $this->satuan_model->listing();

        $valid = $this->form_validation;

        $valid->set_rules('nama_pelanggan', 'Nama Pelanggan', 'required',
        array( 'required' => '%s Jangan lupa untuk di isi'));

        $valid->set_rules('telp_pelanggan', 'Nomor Pelanggan', 'required',
        array( 'required' => '%s Jangan lupa untuk di isi'));

        $valid->set_rules('alamat_pelanggan', 'Alamat Pelanggan', 'required',
        array( 'required' => '%s Jangan lupa untuk di isi'));

        $valid->set_rules('qty', 'Kuantitas Produk', 'required',
        array( 'required' => '%s Jangan lupa untuk di isi'));

        if ($valid->run()===FALSE) {
            $data = array(  'title' => 'Tambah Transaksi',
                            'satuan' => $satuan,
                            'produk' => $produk,
                            'isi' => 'produk/transaksi'
                        );
            $this->load->view('layout/wrapper', $data, FALSE);

        }else {
            $captcha_response = trim($this->input->post('g-recaptcha-response'));

            
            if($captcha_response != ''){
                $keySecret = '6LeBn38dAAAAAOhGRd_oKQjCsp8nnCX7De7ifyKh';

                $check = array(
                    'secret'		=>	$keySecret,
                    'response'		=>	$this->input->post('g-recaptcha-response')
                );

                $startProcess = curl_init();

                curl_setopt($startProcess, CURLOPT_URL, "https://www.google.com/recaptcha/api/siteverify");

                curl_setopt($startProcess, CURLOPT_POST, true);

                curl_setopt($startProcess, CURLOPT_POSTFIELDS, http_build_query($check));

                curl_setopt($startProcess, CURLOPT_SSL_VERIFYPEER, false);

                curl_setopt($startProcess, CURLOPT_RETURNTRANSFER, true);

                $receiveData = curl_exec($startProcess);

                $finalResponse = json_decode($receiveData, true);

                if($finalResponse['success']){
                    $jumlah_awal = $produk_detail->harga_pokok * $this->input->post('qty');
                    $jumlah_akhir = $produk_detail->harga_jual * $this->input->post('qty');
                    $margin_transaksi = $jumlah_akhir - $jumlah_awal;

                    $i = $this->input;
                    $data = array(  'id_produk' => $id_produk,
                                    'id_satuan' => $i->post('id_satuan'),
                                    'nama_pelanggan' => $i->post('nama_pelanggan'),
                                    'alamat_pelanggan' => $i->post('alamat_pelanggan'),
                                    'telp_pelanggan' => $i->post('telp_pelanggan'),
                                    'qty' => $i->post('qty'),
                                    'jumlah_awal'  => $jumlah_awal, 
                                    'jumlah_akhir'  => $jumlah_akhir,
                                    'margin_transaksi' => $margin_transaksi
                                );
                    $this->transaksi_model->tambah($data);
                    $this->session->set_flashdata('sukses', 'Transaksi telah berhasil !!');
                    redirect(base_url('produk'),'refresh');
                }else{
                    $this->session->set_flashdata('error', 'Transaksi Gagal');
                }
            }else{
                $this->session->set_flashdata('error', 'Captcha Bermasalah');
                redirect(base_url('produk/transaksi/'.$produk->id_produk),'refresh');
            }
        }
    }
        
}
        
    /* End of file  Produk.php */
        
                            