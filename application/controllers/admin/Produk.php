<?php 
        
defined('BASEPATH') OR exit('No direct script access allowed');
        
class Produk extends CI_Controller {
    
    public function __construct()
    {
        parent::__construct();
        $this->load->model('produk_model');
        $this->load->model('jenis_model');
        $this->load->model('merk_model');
        $this->load->model('ekspedisi_model');
        $this->admin_login->cek_login();
    }
    

    public function index()
    {
        $produk = $this->produk_model->listing();

        $data = array(  'title'     => 'Daftar Produk',
                        'produk'    => $produk,
                        'isi'       => 'admin/produk/list'
                    );
                    
        $this->load->view('admin/layout/wrapper', $data, FALSE);
        
    }


    public function tambah(){
        $jenis = $this->jenis_model->listing();
        $merk = $this->merk_model->listing();
        $ekspedisi  = $this->ekspedisi_model->listing();
        $ekspedisi_detail = $this->ekspedisi_model->detail($this->input->post('id_ekspedisi'));

        $valid = $this->form_validation;

        $valid->set_rules('nama_produk','Nama Produk','required',
            array(  'required'  => '%s harus di isi'));
        
        $valid->set_rules('harga_produk','Harga Pokok','required',
            array( 'required' => '%s harus di isi'));
        
        $valid->set_rules('margin_produk','Margin Produk','required',
            array( 'required' => '%s harus di isi'));
        
        $valid->set_rules('deskripsi_produk','Deskripsi Produk','required',
            array( 'required' => '%s harus di isi'));
        
        if ($valid->run()) {
            $config['upload_path'] = './assets/upload/image/';
            $config['allowed_types'] = 'gif|jpg|png|jpeg';
            $config['max_size'] = '2400'; //Dalam satuan kb
            $config['max_width'] = '2024';
            $config['max_height'] = '2024';

            $this->load->library('upload', $config);

            if (! $this->upload->do_upload('gambar')) {
                $data = array(  'title' =>  'Tambah Produk',
                                'jenis' =>  $jenis,
                                'merk'  =>  $merk,
                                'ekspedisi' => $ekspedisi,
                                'error' => $this->upload->display_errors(),
                                'isi' => 'admin/produk/tambah'
                            );

                $this->load->view('admin/layout/wrapper', $data, FALSE);
            }else {
                $upload_gambar = array('upload_data' => $this->upload->data());
                
                // create thumbnail
                $config['image_library'] = 'gd2';
                $config['source_image'] = './assets/upload/image/'.$upload_gambar['upload_data']['file_name'];
                // Lokal folder thumbnail
                $config['new_image'] = './assets/upload/image/thumbs/';
                $config['create_thumb'] = TRUE;
                $config['maintain_ratio'] = TRUE;
                $config['width'] = 250;//pixel
                $config['height'] = 250;//pixel
                $config['thumb_marker'] ='';

                $this->load->library('image_lib', $config);
                $this->image_lib->resize();
                // end create thumbnail

                // Slug Produk
                $slug_produk = url_title($this->input->post('nama_produk'), 'dash', TRUE);
                $harga_pokok = $this->input->post('harga_produk') + $this->input->post('margin_produk');
                $harga_jual = $this->input->post('harga_produk') + $this->input->post('margin_produk') + $ekspedisi_detail->harga_ekspedisi; 
                
                $i = $this->input;
                $data = array(  'id_user'       => $this->session->userdata('id_user'),
                                'id_jenis'      => $i->post('id_jenis'),
                                'id_merk'       => $i->post('id_merk'),
                                'id_ekspedisi'  => $i->post('id_ekspedisi'),
                                'nama_produk'   => $i->post('nama_produk'),
                                'slug_produk'   => $slug_produk,
                                'gambar'        => $upload_gambar['upload_data']['file_name'],
                                'harga_produk'  => $i->post('harga_produk'),
                                'harga_pokok'   => $harga_pokok,
                                'margin_produk'    => $i->post('margin_produk'),
                                'harga_jual'       => $harga_jual, 
                                'deskripsi_produk' => $i->post('deskripsi_produk')
                            );
                $this->produk_model->tambah($data);
                $this->session->set_flashdata('sukses', 'Data telah ditambah');
                redirect(base_url('admin/produk'),'refresh');
            }
        }
        $data = array(  'title' => 'Tambah Produk',
                        'jenis' => $jenis,
                        'merk'  => $merk,
                        'ekspedisi' => $ekspedisi,
                        'isi'   => 'admin/produk/tambah'
                    );

        $this->load->view('admin/layout/wrapper', $data, FALSE);
        
    }

    public function edit($id_produk){
        $produk = $this->produk_model->detail($id_produk);
        $jenis = $this->jenis_model->listing();
        $merk = $this->merk_model->listing();
        $ekspedisi = $this->ekspedisi_model->listing();
        $ekspedisi_detail = $this->ekspedisi_model->detail($this->input->post('id_ekspedisi'));

        $valid = $this->form_validation;

        $valid->set_rules('nama_produk','Nama Produk','required',
        array( 'required' => '%s harus di isi'));

        $valid->set_rules('harga_produk','Harga Produk','required',
        array( 'required' => '%s harus di isi'));

        $valid->set_rules('margin_produk','Margin Produk','required',
        array( 'required' => '%s harus di isi'));

        $valid->set_rules('deskripsi_produk','Deskripsi Produk','required',
            array( 'required' => '%s harus di isi'));

        if ($valid->run()) {
            if (! empty($_FILES['gambar']['name'])) {
                $config['upload_path'] = './assets/upload/image/';
                $config['allowed_types'] = 'gif|jpg|png|jpeg';
                $config['max_size'] = '2400'; //Dalam satuan kb
                $config['max_width'] = '2024';
                $config['max_height'] = '2024';

                $this->load->library('upload', $config);
                if (! $this->upload->do_upload('gambar')) {
                    $data = array(  'title' => 'Edit Produk',
                                    'produk' => $produk,
                                    'jenis' => $jenis,
                                    'merk' => $merk,
                                    'ekspedisi' => $ekspedisi,
                                    'error' => $this->upload->display_errors(),
                                    'isi' => 'admin/produk/edit'
                                );
                    $this->load->view('admin/layout/wrapper', $data, FALSE);
                }else {
                    $upload_gambar = array('upload_data' => $this->upload->data());

                    // create thumbnail
                    $config['image_library'] = 'gd2';
                    $config['source_image'] = './assets/upload/image/'.$upload_gambar['upload_data']['file_name'];
                    // Lokal folder thumbnail
                    $config['new_image'] = './assets/upload/image/thumbs/';
                    $config['create_thumb'] = TRUE;
                    $config['maintain_ratio'] = TRUE;
                    $config['width'] = 250;//pixel
                    $config['height'] = 250;//pixel
                    $config['thumb_marker'] ='';

                    $this->load->library('image_lib', $config);
                    $this->image_lib->resize();
                    // end create thumbnail

                    // Slug Produk
                    $slug_produk = url_title($this->input->post('nama_produk'), 'dash', TRUE);
                    $harga_pokok = $this->input->post('harga_produk') + $this->input->post('margin_produk'); 
                    $harga_jual = $this->input->post('harga_produk') + $this->input->post('margin_produk') + $ekspedisi_detail->harga_ekspedisi; 

                    $i = $this->input;
                    $data = array(  'id_produk' => $id_produk,
                                    'id_user' => $this->session->userdata('id_user'),
                                    'id_jenis' => $i->post('id_jenis'),
                                    'id_merk' => $i->post('id_merk'),
                                    'id_ekspedisi' => $i->post('id_ekspedisi'),
                                    'nama_produk' => $i->post('nama_produk'),
                                    'slug_produk' => $slug_produk,
                                    'gambar' => $upload_gambar['upload_data']['file_name'],
                                    'harga_produk' => $i->post('harga_produk'),
                                    'harga_pokok' => $harga_pokok,
                                    'margin_produk' => $i->post('margin_produk'),
                                    'harga_jual'    => $harga_jual,
                                    'deskripsi_produk' => $i->post('deskripsi_produk')
                                );
                    $this->produk_model->edit($data);
                    $this->session->set_flashdata('sukses', 'Data telah diedit');
                    redirect(base_url('admin/produk'),'refresh');
                }
            }else {
                // Slug Produk
                $slug_produk = url_title($this->input->post('nama_produk'), 'dash', TRUE);
                $harga_pokok = $this->input->post('harga_produk') + $this->input->post('margin_produk');
                $harga_jual = $this->input->post('harga_produk') + $this->input->post('margin_produk') + $ekspedisi_detail->harga_ekspedisi; 
                
                $i = $this->input;
                $data = array(  'id_produk' => $id_produk,
                                'id_user' => $this->session->userdata('id_user'),
                                'id_jenis' => $i->post('id_jenis'),
                                'id_merk' => $i->post('id_merk'),
                                'id_ekspedisi' => $i->post('id_ekspedisi'),
                                'nama_produk' => $i->post('nama_produk'),
                                'slug_produk' => $slug_produk,
                                'harga_produk' => $i->post('harga_produk'),
                                'harga_pokok' => $harga_pokok,
                                'margin_produk' => $i->post('margin_produk'),
                                'harga_jual'    => $harga_jual,
                                'deskripsi_produk' => $i->post('deskripsi_produk')
                            );
                $this->produk_model->edit($data);
                $this->session->set_flashdata('sukses', 'Data telah diedit');
                redirect(base_url('admin/produk'),'refresh');
            }
        }
        $data = array(  'title' => 'Edit Produk',
                        'produk' => $produk,
                        'jenis' => $jenis,
                        'merk' => $merk,
                        'ekspedisi' => $ekspedisi,
                        'isi' => 'admin/produk/edit'
                    );
        $this->load->view('admin/layout/wrapper', $data, FALSE);
    }

    public function delete($id_produk){
        $produk = $this->produk_model->detail($id_produk);
        unlink('./assets/upload/image/'.$produk->gambar);
        unlink('./assets/upload/image/thumbs/'.$produk->gambar);

        $data = array('id_produk' => $id_produk);
        $this->produk_model->delete($data);
        $this->session->set_flashdata('sukses', 'Data telah dihapus');
        redirect(base_url('admin/produk'),'refresh');
    }
        
}
        
    /* End of file  produk.php */
        
                            