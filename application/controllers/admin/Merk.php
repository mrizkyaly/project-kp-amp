<?php 
        
defined('BASEPATH') OR exit('No direct script access allowed');
        
class Merk extends CI_Controller {

    
    public function __construct()
    {
        parent::__construct();
        $this->load->model('merk_model');
        $this->admin_login->cek_login();
    }

    public function index(){
        $merk = $this->merk_model->listing();

        $data = array(  'title' => 'Daftar Merk',
                        'merk'  => $merk,
                        'isi'   => 'admin/merk/list'
                    );
        
        $this->load->view('admin/layout/wrapper', $data, FALSE);
        
    }

    public function tambah(){

        $valid = $this->form_validation;

        $valid->set_rules('nama_merk', 'Nama Merk', 'required',
        array( 'nama_merk'  => '%s harus di isi'));

        if ($valid->run()===FALSE) {
            $data = array(  'title' => 'Tambah Merk',
                            'isi'   => 'admin/merk/tambah'
                        );

            $this->load->view('admin/layout/wrapper', $data, FALSE);
            
        }else {
            $i = $this->input;
            $data = array(  'nama_merk' => $i->post('nama_merk'));
            $this->merk_model->tambah($data);
            $this->session->set_flashdata('sukses', 'Data telah ditambah');
            redirect(base_url('admin/merk'),'refresh');
        }
    }

    public function edit($id_merk){
        $merk = $this->merk_model->detail($id_merk);

        $valid = $this->form_validation;

        $valid->set_rules('nama_merk', 'Nama Merk', 'required',
        array( 'nama_merk' => '%s harus di isi'));

        if ($valid->run()===FALSE) {
            $data = array(  'title' => 'Edit Merk',
                            'merk'  => $merk,
                            'isi'   => 'admin/merk/edit'
                        );

            $this->load->view('admin/layout/wrapper', $data, FALSE);
            
        }else {
            $i = $this->input;
            $data = array(  'id_merk'   => $id_merk,
                            'nama_merk' => $i->post('nama_merk')
                        );
            $this->merk_model->edit($data);
            $this->session->set_flashdata('sukses', 'Data telah ditambah');
            redirect(base_url('admin/merk'),'refresh');
        }
    }

    public function delete($id_merk){
        $data = array('id_merk' => $id_merk);
        $this->merk_model->delete($data);
        $this->session->set_flashdata('error', 'Data telah di hapus...');
        redirect(base_url('admin/merk'),'refresh');
    }
    
        
}
        
    /* End of file  merk.php */
        
                            