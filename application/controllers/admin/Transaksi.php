<?php 
        
defined('BASEPATH') OR exit('No direct script access allowed');

require('./application/third_party/vendor/autoload.php');

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
        
class Transaksi extends CI_Controller {

    
    public function __construct()
    {
        parent::__construct();
        $this->load->model('transaksi_model');
        $this->load->model('produk_model');
        $this->load->model('satuan_model');
        $this->admin_login->cek_login();
    }
    
    public function index()
    {
        $transaksi = $this->transaksi_model->listing();

        $data = array(  'title'  => 'Daftar Transaksi',
                        'transaksi' => $transaksi,
                        'isi'   => 'admin/transaksi/list'
                    );
        $this->load->view('admin/layout/wrapper', $data, FALSE);
        
    }

    public function tambah()
    {
        $produk = $this->produk_model->listing();
        $produk_detail = $this->produk_model->detail($this->input->post('id_produk'));
        $satuan  = $this->satuan_model->listing();

        $valid = $this->form_validation;

        $valid->set_rules('nama_pelanggan', 'Nama Pelanggan', 'required',
        array( 'nama_pelanggan' => '%s harus di isi'));

        $valid->set_rules('telp_pelanggan', 'Nomor Pelanggan', 'required',
        array( 'telp_pelanggan' => '%s harus di isi'));

        $valid->set_rules('alamat_pelanggan', 'Alamat Pelanggan', 'required',
        array( 'Alamat_pelanggan' => '%s harus di isi'));
        
        $valid->set_rules('qty', 'QTY', 'required',
        array( 'qty' => '%s harus di isi'));
        
        $valid->set_rules('catatan', 'Catatan', 'required',
        array( 'catatan' => '%s harus di isi'));

        if ($valid->run()===FALSE) {
            $data = array(  'title' => 'Tambah Transaksi',
                            'produk'    => $produk,
                            'satuan'    => $satuan,
                            'isi'   => 'admin/transaksi/tambah'
                        );
            $this->load->view('admin/layout/wrapper', $data, FALSE);
            
        }else {
            $jumlah_awal = $produk_detail->harga_pokok * $this->input->post('qty');
            $jumlah_akhir = $produk_detail->harga_jual * $this->input->post('qty');
            $margin_transaksi = $jumlah_akhir - $jumlah_awal;

            $i = $this->input;
            $data = array(  'id_produk'     => $i->post('id_produk'),
                            'id_satuan'     => $i->post('id_satuan'),
                            'nama_pelanggan'    => $i->post('nama_pelanggan'),
                            'alamat_pelanggan'  => $i->post('alamat_pelanggan'),
                            'telp_pelanggan'    => $i->post('telp_pelanggan'),
                            'qty'   => $i->post('qty'),
                            'jumlah_awal'  => $jumlah_awal, 
                            'jumlah_akhir'  => $jumlah_akhir,
                            'margin_transaksi' => $margin_transaksi, 
                            'catatan'    => $i->post('catatan')
                        );
            $this->transaksi_model->tambah($data);
            $this->session->set_flashdata('sukses', 'Data telah ditambah');
            redirect(base_url('admin/transaksi'),'refresh');
        }
    }

    public function edit($id_transaksi)
    {
        $transaksi = $this->transaksi_model->detail($id_transaksi);
        $produk = $this->produk_model->listing();
        $produk_detail = $this->produk_model->detail($this->input->post('id_produk'));
        $satuan = $this->satuan_model->listing();

        $valid = $this->form_validation;

        $valid->set_rules('nama_pelanggan', 'Nama Pelanggan', 'required',
        array( 'nama_pelanggan' => '%s harus di isi'));

        $valid->set_rules('telp_pelanggan', 'Nomor Pelanggan', 'required',
        array( 'telp_pelanggan' => '%s harus di isi'));

        $valid->set_rules('alamat_pelanggan', 'Alamat Pelanggan', 'required',
        array( 'Alamat_pelanggan' => '%s harus di isi'));

        $valid->set_rules('qty', 'QTY', 'required',
        array( 'qty' => '%s harus di isi'));

        $valid->set_rules('catatan', 'Catatan', 'required',
        array( 'catatan' => '%s harus di isi'));

        if ($valid->run()===FALSE) {
            $data = array(  'title' => 'Edit Transaksi',
                            'transaksi' => $transaksi,
                            'produk'    => $produk,
                            'satuan'    => $satuan,
                            'isi'       => 'admin/transaksi/edit'
                        );
            $this->load->view('admin/layout/wrapper', $data, FALSE);
            
        }else {
            $jumlah_awal = $produk_detail->harga_pokok * $this->input->post('qty');
            $jumlah_akhir = $produk_detail->harga_jual * $this->input->post('qty');
            $margin_transaksi = $jumlah_akhir - $jumlah_awal;

            $i = $this->input;
            $data = array(  'id_transaksi' => $id_transaksi,
                            'id_satuan'    => $i->post('id_satuan'),
                            'id_produk' => $i->post('id_produk'),
                            'nama_pelanggan' => $i->post('nama_pelanggan'),
                            'alamat_pelanggan' => $i->post('alamat_pelanggan'),
                            'telp_pelanggan' => $i->post('telp_pelanggan'),
                            'qty' => $i->post('qty'),
                            'jumlah_awal'  => $jumlah_awal, 
                            'jumlah_akhir'  => $jumlah_akhir,
                            'margin_transaksi' => $margin_transaksi, 
                            'catatan' => $i->post('catatan')
                        );
            $this->transaksi_model->edit($data);
            $this->session->set_flashdata('sukses', 'Data telah diedit');
            redirect(base_url('admin/transaksi'),'refresh');
        }
    }

    public function delete($id_transaksi)
    {
        $data = array('id_transaksi' => $id_transaksi);
        $this->transaksi_model->delete($data);
        $this->session->set_flashdata('error', 'Data telah di hapus...');
        redirect(base_url('admin/transaksi'),'refresh');
    }

    public function export(){
        $data_export = $this->transaksi_model->listing();

        $spreadsheet = new Spreadsheet;

        $spreadsheet->setActiveSheetIndex(0)
                    ->setCellValue('A1', 'No')
                    ->setCellValue('B1', 'Tanggal')
                    ->setCellValue('C1', 'Nama')
                    ->setCellValue('D1', 'Alamat')
                    ->setCellValue('E1', 'No Telp')
                    ->setCellValue('F1', 'Nama Barang')
                    ->setCellValue('G1', 'QTY')
                    ->setCellValue('H1', 'Satuan')
                    ->setCellValue('I1', 'Harga Pokok')
                    ->setCellValue('J1', 'Harga Jual')
                    ->setCellValue('K1', 'Jumlah Awal')
                    ->setCellValue('L1', 'Jumlah Akhir')
                    ->setCellValue('M1', 'Margin Transaksi')
                    ->setCellValue('N1', 'Catatan');
        
        $kolom = 2;
        $nomor = 1;

        foreach ($data_export as $transaksi) {
            $spreadsheet->setActiveSheetIndex(0)
                        ->setCellValue('A'. $kolom, $nomor)
                        ->setCellValue('B'. $kolom, $transaksi->tgl_transaksi)
                        ->setCellValue('C'. $kolom, $transaksi->nama_pelanggan)
                        ->setCellValue('D'. $kolom, $transaksi->alamat_pelanggan)
                        ->setCellValue('E'. $kolom, $transaksi->telp_pelanggan)
                        ->setCellValue('F'. $kolom, $transaksi->nama_produk)
                        ->setCellValue('G'. $kolom, $transaksi->qty)
                        ->setCellValue('H'. $kolom, $transaksi->nama_satuan)
                        ->setCellValue('I'. $kolom, $transaksi->harga_pokok)
                        ->setCellValue('J'. $kolom, $transaksi->harga_jual)
                        ->setCellValue('K'. $kolom, $transaksi->jumlah_awal)
                        ->setCellValue('L'. $kolom, $transaksi->jumlah_akhir)
                        ->setCellValue('M'. $kolom, $transaksi->margin_transaksi)
                        ->setCellValue('N'. $kolom, $transaksi->catatan);
            $kolom++;
            $nomor++;
        }
        
        $writer = new Xlsx($spreadsheet);

        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="Laporan Penjualan.xlsx"');
        header('Cache-Control: max-age=0');

        $writer->save('php://output');
    }
        
}
        
    /* End of file  Transaksi.php */
        
                            