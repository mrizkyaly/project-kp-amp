<?php 
        
defined('BASEPATH') OR exit('No direct script access allowed');
        
class Ekspedisi extends CI_Controller {

    
    public function __construct()
    {
        parent::__construct();
        $this->load->model('ekspedisi_model');
        $this->admin_login->cek_login();
    }

    public function index(){
        $ekspedisi = $this->ekspedisi_model->listing();

        $data = array(  'title'     => 'Daftar Ekspedisi',
                        'ekspedisi' => $ekspedisi,
                        'isi'       => 'admin/ekspedisi/list'
                    );
                    
        $this->load->view('admin/layout/wrapper', $data, FALSE);
    }

    public function tambah(){
        $valid = $this->form_validation;

        $valid->set_rules('kota_tujuan','Kota Tujuan','required',
        array( 'kota_tujuan' =>'%s harus diisi'));

        $valid->set_rules('harga_ekspedisi','Harga Ekspedisi','required',
        array( 'harga_ekspedisi' =>'%s harus diisi'));

        if ($valid->run()===FALSE) {
            $data = array(  'title' => 'Tambah Ekspedisi',
                            'isi'   =>  'admin/ekspedisi/tambah'
                        );
            $this->load->view('admin/layout/wrapper', $data, FALSE);
            
        }else {
            $i = $this->input;
            $data = array(  'kota_tujuan' => $i->post('kota_tujuan'),
                            'harga_ekspedisi' => $i->post('harga_ekspedisi')
                        );
            $this->ekspedisi_model->tambah($data);
            $this->session->set_flashdata('sukses', 'Data telah ditambah');
            redirect(base_url('admin/ekspedisi'),'refresh');
        }
    }

    public function edit($id_ekspedisi){
        $ekspedisi = $this->ekspedisi_model->detail($id_ekspedisi);

        $valid = $this->form_validation;

        $valid->set_rules('kota_tujuan','Kota Tujuan','required',
        array( 'kota_tujuan' =>'%s harus diisi'));

        $valid->set_rules('harga_ekspedisi','Harga Ekspedisi','required',
        array( 'harga_ekspedisi' =>'%s harus diisi'));

        if ($valid->run()===FALSE) {
            $data = array(  'title'     => 'Edit Ekspedisi',
                            'ekspedisi' => $ekspedisi,
                            'isi'       => 'admin/ekspedisi/edit'
                        );
            $this->load->view('admin/layout/wrapper', $data, FALSE);
            
        }else {
            $i = $this->input;
            $data = array(  'id_ekspedisi'  => $id_ekspedisi,
                            'kota_tujuan'   => $i->post('kota_tujuan'),
                            'harga_ekspedisi' => $i->post('harga_ekspedisi')
                        );
            $this->ekspedisi_model->edit($data);
            $this->session->set_flashdata('sukses', 'Data telah ditambah');
            redirect(base_url('admin/ekspedisi'),'refresh');
        }
    }

    public function delete($id_ekspedisi){
        $data = array('id_ekspedisi' => $id_ekspedisi);
        $this->ekspedisi_model->delete($data);
        $this->session->set_flashdata('error', 'Data telah di hapus...');
        redirect(base_url('admin/ekspedisi'),'refresh');
    }
    
        
}
        
    /* End of file  Ekspedisi.php */
        
                            