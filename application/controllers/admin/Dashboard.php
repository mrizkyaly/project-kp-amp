<?php 
        
defined('BASEPATH') OR exit('No direct script access allowed');
        
class Dashboard extends CI_Controller {

    
    public function __construct()
    {
        parent::__construct();
        $this->load->model('ekspedisi_model');
        $this->load->model('produk_model');
        $this->load->model('transaksi_model');
        $this->load->model('user_model');
        $this->admin_login->cek_login();
    }
    

    public function index()
    {
        $jumlah_ekspedisi = $this->ekspedisi_model->total_ekspedisi();
        $jumlah_produk = $this->produk_model->total_produk();
        $jumlah_user = $this->user_model->total_user();
        $jumlah_transaksi = $this->transaksi_model->total_transaksi();

        $data = array(  'title' => 'Dashboard',
                        'jumlah_ekspedisi'  => $jumlah_ekspedisi->total,
                        'jumlah_produk' => $jumlah_produk->total,
                        'jumlah_user'   => $jumlah_user->total,
                        'jumlah_transaksi'  => $jumlah_transaksi->total,
                        'isi'   => 'admin/dashboard/list'
                    );
        
        $this->load->view('admin/layout/wrapper', $data, FALSE);
        
        
    }
        
}
        
    /* End of file  dashboard.php */
        
                            