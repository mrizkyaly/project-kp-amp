<?php 
        
defined('BASEPATH') OR exit('No direct script access allowed');
        
class Register extends CI_Controller {

    public function index()
    {
        // Validasi data yang mau di input
        $valid = $this->form_validation;

        $valid->set_rules('nama_user', 'Nama User', 'required',
                array( 'required' => '%s harus di isi'
            ));

        $valid->set_rules('nomor','Nomor','required',
                array( 'required' => '%s harus di isi'
            ));

        $valid->set_rules('username','Username','required|min_length[5]|max_length[32]|is_unique[users.username]',
                array( 'required' => '%s harus di isi',
                'min_length' => '%s minimal 5 karakter',
                'max_length' => '%s maksimal 32 karakter',
                'is_unique' => '%s sudah ada silahkan buat username baru'
            ));

        $valid->set_rules('password','Password','required',
                array( 'required' => '%s harus di isi'
            ));

        if ($valid->run()) {
            $config['upload_path'] = './assets/upload/image/';
            $config['allowed_types'] = 'gif|jpg|png|jpeg';
            $config['max_size'] = '2400'; //Dalam satuan kb
            $config['max_width'] = '2024';
            $config['max_height'] = '2024';

            $this->load->library('upload', $config);

        if (! $this->upload->do_upload('gambar')) {
            $data = array( 'title' => 'Register Admin',
                            'error' => $this->upload->display_errors()
                    );

            $this->load->view('admin/register/list', $data, FALSE);

        }else {
            $upload_gambar = array('upload_data' => $this->upload->data());

            // create thumbnail
            $config['image_library'] = 'gd2';
            $config['source_image'] = './assets/upload/image/'.$upload_gambar['upload_data']['file_name'];
            // Lokal folder thumbnail
            $config['new_image'] = './assets/upload/image/thumbs/';
            $config['create_thumb'] = TRUE;
            $config['maintain_ratio'] = TRUE;
            $config['width'] = 250;//pixel
            $config['height'] = 250;//pixel
            $config['thumb_marker'] ='';

            $this->load->library('image_lib', $config);
            $this->image_lib->resize();
            // end create thumbnail

            $i = $this->input;
            $data = array(  'nama_user' => $i->post('nama_user'),
                            'jabatan' => $i->post('jabatan'),
                            'nomor' => $i->post('nomor'),
                            'username' => $i->post('username'),
                            'password' => SHA1($i->post('password')),
                            'gambar' => $upload_gambar['upload_data']['file_name'],
                        );
                $this->user_model->tambah($data);
                $this->session->set_flashdata('sukses', 'Data telah ditambahkan...');
                redirect(base_url('root'),'refresh');
            }
        }
        
        $data = array(  'title' => 'Register Admin');
        $this->load->view('admin/register/list', $data, FALSE);
    }
        
}
        
    /* End of file  Register.php */
        
                            