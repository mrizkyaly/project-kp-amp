<?php 
        
defined('BASEPATH') OR exit('No direct script access allowed');
        
class Satuan extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('satuan_model');
        $this->admin_login->cek_login();
    }

    public function index(){
        $satuan = $this->satuan_model->listing();

        $data = array( 'title' => 'Daftar Satuan',
                        'satuan' => $satuan,
                        'isi' => 'admin/satuan/list'
                    );

        $this->load->view('admin/layout/wrapper', $data, FALSE);

    }

    public function tambah(){

        $valid = $this->form_validation;

        $valid->set_rules('nama_satuan', 'Nama Satuan', 'required',
        array( 'nama_satuan' => '%s harus di isi'));

        if ($valid->run()===FALSE) {
            $data = array(  'title' => 'Tambah Satuan',
                            'isi' => 'admin/satuan/tambah'
                        );

            $this->load->view('admin/layout/wrapper', $data, FALSE);

        }else {
            $i = $this->input;
            $data = array( 'nama_satuan' => $i->post('nama_satuan'));
            $this->satuan_model->tambah($data);
            $this->session->set_flashdata('sukses', 'Data telah ditambah');
            redirect(base_url('admin/satuan'),'refresh');
        }
    }

    public function edit($id_satuan){
        $satuan = $this->satuan_model->detail($id_satuan);

        $valid = $this->form_validation;

        $valid->set_rules('nama_satuan', 'Nama Satuan', 'required',
        array( 'nama_satuan' => '%s harus di isi'));

        if ($valid->run()===FALSE) {
            $data = array(  'title' => 'Edit Satuan',
                            'satuan' => $satuan,
                            'isi' => 'admin/satuan/edit'
                        );

            $this->load->view('admin/layout/wrapper', $data, FALSE);

        }else {
            $i = $this->input;
            $data = array(  'id_satuan' => $id_satuan,
                            'nama_satuan' => $i->post('nama_satuan')
                        );
            $this->satuan_model->edit($data);
            $this->session->set_flashdata('sukses', 'Data telah ditambah');
            redirect(base_url('admin/satuan'),'refresh');
        }
    }
    

    public function delete($id_satuan){
        $data = array('id_satuan' => $id_satuan);
        $this->satuan_model->delete($data);
        $this->session->set_flashdata('error', 'Data telah di hapus...');
        redirect(base_url('admin/satuan'),'refresh');
    }
        
}
        
    /* End of file  Satuan.php */
        
                            