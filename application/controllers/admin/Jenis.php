<?php 
        
defined('BASEPATH') OR exit('No direct script access allowed');
        
class Jenis extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('jenis_model');
        $this->admin_login->cek_login();
    }

    public function index(){
        $jenis = $this->jenis_model->listing();

        $data = array( 'title' => 'Daftar Jenis',
                        'jenis' => $jenis,
                        'isi' => 'admin/jenis/list'
                    );

        $this->load->view('admin/layout/wrapper', $data, FALSE);

    }

    public function tambah(){

        $valid = $this->form_validation;

        $valid->set_rules('nama_jenis', 'Nama Jenis', 'required',
        array( 'nama_jenis' => '%s harus di isi'));

        if ($valid->run()===FALSE) {
            $data = array(  'title' => 'Tambah Jenis',
                            'isi' => 'admin/jenis/tambah'
                        );

            $this->load->view('admin/layout/wrapper', $data, FALSE);

        }else {
            $i = $this->input;
            $data = array( 'nama_jenis' => $i->post('nama_jenis'));
            $this->jenis_model->tambah($data);
            $this->session->set_flashdata('sukses', 'Data telah ditambah');
            redirect(base_url('admin/jenis'),'refresh');
        }
    }

    public function edit($id_jenis){
        $jenis = $this->jenis_model->detail($id_jenis);

        $valid = $this->form_validation;

        $valid->set_rules('nama_jenis', 'Nama Jenis', 'required',
        array( 'nama_jenis' => '%s harus di isi'));

        if ($valid->run()===FALSE) {
            $data = array(  'title' => 'Edit Jenis',
                            'jenis' => $jenis,
                            'isi' => 'admin/jenis/edit'
                        );

            $this->load->view('admin/layout/wrapper', $data, FALSE);

        }else {
            $i = $this->input;
            $data = array(  'id_jenis' => $id_jenis,
                            'nama_jenis' => $i->post('nama_jenis')
                        );
            $this->jenis_model->edit($data);
            $this->session->set_flashdata('sukses', 'Data telah ditambah');
            redirect(base_url('admin/jenis'),'refresh');
        }
    }
    

    public function delete($id_jenis){
        $data = array('id_jenis' => $id_jenis);
        $this->jenis_model->delete($data);
        $this->session->set_flashdata('error', 'Data telah di hapus...');
        redirect(base_url('admin/jenis'),'refresh');
    }
        
}
        
    /* End of file  Jenis.php */
        
                            