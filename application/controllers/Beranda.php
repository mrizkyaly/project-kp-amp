<?php 
        
defined('BASEPATH') OR exit('No direct script access allowed');
        
class Beranda extends CI_Controller {

    
    public function __construct()
    {
        parent::__construct();
        $this->load->model('produk_model');
    }
    

    public function index()
    {
        $produk = $this->produk_model->listing();

        $data = array(  'title' => 'Beranda',
                        'produk' => $produk,
                        'isi' => 'beranda/list'
                );
        $this->load->view('layout/wrapper', $data, FALSE);
    }
        
}
        
    /* End of file  Beranda.php */
        
                            