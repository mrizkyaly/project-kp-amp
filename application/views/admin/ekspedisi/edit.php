<!-- Main Content -->
<div class="main-content">
    <section class="section">
        <div class="section-header">
            <h1>Data Ekspedisi</h1>
            <div class="section-header-breadcrumb breadcrumb bg-success text-white-all">
                <div class="breadcrumb-item active"><a href="<?php echo base_url('admin/dashboard') ?>">Dashboard</a>
                </div>
                <div class="breadcrumb-item"><a href="<?php echo base_url('admin/ekspedisi') ?>">Ekspedisi</a></div>
                <div class="breadcrumb-item"><?php echo $title ?></div>
            </div>
        </div>
        <div class="section-body">
            <div class="row">
                <div class="col-lg-12">
                    <div class="card card-success">
                        <div class="card-body">
                            <?php
                        //Error upload
                        if (isset($error)) {
                            echo '<div class="alert alert-warning">';
                                echo $error;
                                echo '</div>';
                            }
                            // Notifikasi error
                            echo validation_errors('<div class="alert alert-warning alert-dismissible show fade">','</div>');
                            // Form open
                            echo form_open_multipart(base_url('admin/ekspedisi/edit/'.$ekspedisi->id_ekspedisi), 'class="form-horizontal"');
                        ?>
                            <div class="row d-flex">
                                <div class="col-12">
                                    <div class="form-group">
                                        <label class="control-label">Kota Tujuan</label>
                                        <input type="text" name="kota_tujuan" class="form-control" placeholder=""
                                            value="<?php echo $ekspedisi->kota_tujuan ?>" required>
                                    </div>
                                </div>
                                <div class="col-12">
                                    <div class="form-group">
                                        <label class="control-label">Harga</label>
                                        <input type="text" name="harga_ekspedisi" class="form-control" placeholder=""
                                            value="<?php echo $ekspedisi->harga_ekspedisi ?>" required>
                                    </div>
                                </div>
                                <div class="col-12">
                                    <button class="btn btn-success btn-md btn-icon icon-left btn-block" name="submit"
                                        type="submit">
                                        <i class="fas fa-plus"></i> Tambah Ekspedisi
                                    </button>
                                </div>
                            </div>
                            <?php echo form_close(); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>