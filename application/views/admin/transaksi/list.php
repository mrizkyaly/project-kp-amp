<!-- Main Content -->
<div class="main-content">
    <section class="section">
        <div class="section-header">
            <h1>Transaksi - AMP</h1>
            <div class="section-header-breadcrumb breadcrumb bg-success text-white-all">
                <div class="breadcrumb-item active"><a href="<?php echo base_url('admin/dashboard') ?>">Dashboard</a>
                </div>
                <div class="breadcrumb-item"><a href="<?php echo base_url('admin/transaksi') ?>">Transaksi</a></div>
                <div class="breadcrumb-item"><?php echo $title ?></div>
            </div>
        </div>
        <div class="section-body">
            <div class="row mb-1">
                <div class="col-12">
                    <?php 
                        // Notifikasi sukses
                        if ($this->session->flashdata('sukses')) {
                            echo '<p class="alert alert-success">';
                            echo $this->session->flashdata('sukses');
                        }elseif ($this->session->flashdata('error')) {
                            echo '<p class="alert alert-danger">';
                                echo $this->session->flashdata('error');
                        }
                    ?>
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <div class="card card-success">
                        <div class="card-header">
                            <h4>List Transaksi</h4>
                            <div class="card-header-action">
                                <a href="<?php echo base_url('admin/transaksi/tambah') ?>"
                                    class="btn btn-success btn-icon icon-left"><i class="fas fa-plus"></i> Tambah
                                    Data
                                </a>
                                <a href="<?php echo base_url('admin/transaksi/export') ?>"
                                    class="btn btn-info btn-icon icon-left"><i class="fas fa-file-excel"></i>
                                    Download Laporan
                                </a>
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table table-striped" id="table-2">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Tanggal</th>
                                            <th>Nama</th>
                                            <th>Alamat</th>
                                            <th>Nomor Telp</th>
                                            <th>Nama Barang</th>
                                            <th>Qty</th>
                                            <th>Satuan</th>
                                            <th>Harga Pokok (RP)</th>
                                            <th>Harga Jual (RP)</th>
                                            <th>Jumlah Awal (RP)</th>
                                            <th>Jumlah Akhir (RP)</th>
                                            <th>Margin Transaksi (RP)</th>
                                            <th>Catatan</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php $no = 1; foreach ($transaksi as $transaksi) {?>
                                        <tr>
                                            <td><?php echo $no ?></td>
                                            <td><?php echo $transaksi->tgl_transaksi ?></td>
                                            <td><?php echo $transaksi->nama_pelanggan ?></td>
                                            <td><?php echo $transaksi->alamat_pelanggan ?></td>
                                            <td><?php echo $transaksi->telp_pelanggan ?></td>
                                            <td><?php echo $transaksi->nama_produk ?></td>
                                            <td><?php echo $transaksi->qty ?></td>
                                            <td><?php echo $transaksi->nama_satuan ?></td>
                                            <td>
                                                <?php echo  number_format($transaksi->harga_pokok,'0', ',', '.')  ?>
                                            </td>
                                            <td>
                                                <?php echo  number_format($transaksi->harga_jual,'0', ',', '.')  ?>
                                            </td>
                                            <td>
                                                <?php echo  number_format($transaksi->jumlah_awal,'0', ',', '.')  ?>
                                            </td>
                                            <td>
                                                <?php echo  number_format($transaksi->jumlah_akhir,'0', ',', '.')  ?>
                                            </td>
                                            <td>
                                                <?php echo  number_format($transaksi->margin_transaksi,'0', ',', '.')  ?>
                                            </td>
                                            <td><?php echo $transaksi->catatan ?></td>
                                            <td>
                                                <a href="<?php echo base_url('admin/transaksi/edit/'.$transaksi->id_transaksi) ?>"
                                                    class="btn btn-warning btn-xs btn-icon icon-left my-2 my-lg-0 mr-2"><i
                                                        class="fa fa-edit"></i>
                                                    Edit</a>
                                                <?php include('delete.php') ?>
                                            </td>
                                        </tr>
                                        <?php $no++; }?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>