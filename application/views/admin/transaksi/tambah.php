<!-- Main Content -->
<div class="main-content">
    <section class="section">
        <div class="section-header">
            <h1>Data Transaksi</h1>
            <div class="section-header-breadcrumb breadcrumb bg-success text-white-all">
                <div class="breadcrumb-item active"><a href="<?php echo base_url('admin/dashboard') ?>">Dashboard</a>
                </div>
                <div class="breadcrumb-item"><a href="<?php echo base_url('admin/transaksi') ?>">Transaksi</a></div>
                <div class="breadcrumb-item"><?php echo $title ?></div>
            </div>
        </div>
        <div class="section-body">
            <div class="row">
                <div class="col-lg-12">
                    <div class="card card-success">
                        <div class="card-body">
                            <?php
                                //Error upload
                                if (isset($error)) {
                                echo '<div class="alert alert-warning">';
                                    echo $error;
                                    echo '</div>';
                                }
                                // Notifikasi error
                                echo validation_errors('<div class="alert alert-warning alert-dismissible show fade">','</div>');
                                // Form open
                                echo form_open_multipart(base_url('admin/transaksi/tambah'), 'class="form-horizontal"');
                            ?>
                            <div class="row d-flex">
                                <div class="col-lg-6 col-12">
                                    <div class="form-group">
                                        <label class="control-label">Nama Pelanggan</label>
                                        <input type="text" name="nama_pelanggan" class="form-control" placeholder=""
                                            value="<?php echo set_value('nama_pelanggan') ?>" required>
                                    </div>
                                </div>
                                <div class="col-lg-6 col-12">
                                    <div class="form-group">
                                        <label class="control-label">Nomor Pelanggan</label>
                                        <input type="text" name="telp_pelanggan" class="form-control" placeholder=""
                                            value="<?php echo set_value('telp_pelanggan') ?>" required>
                                    </div>
                                </div>
                                <div class="col-12">
                                    <div class="form-group">
                                        <label class="control-label">Alamat Pelanggan</label>
                                        <input type="text" name="alamat_pelanggan" class="form-control" placeholder=""
                                            value="<?php echo set_value('alamat_pelanggan') ?>" required>
                                    </div>
                                </div>
                                <div class="col-lg-6 col-12">
                                    <div class="form-group">
                                        <label class="control-label">Nama Produk</label>
                                        <select name="id_produk" class="form-control">
                                            <?php foreach ($produk as $produk) {?>
                                            <option value="<?php echo $produk->id_produk ?>">
                                                <?php echo $produk->nama_produk ?>
                                            </option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-lg-6 col-12">
                                    <div class="form-group">
                                        <label class="control-label">Satuan Produk</label>
                                        <select name="id_satuan" class="form-control">
                                            <?php foreach ($satuan as $satuan) {?>
                                            <option value="<?php echo $satuan->id_satuan ?>">
                                                <?php echo $satuan->nama_satuan ?>
                                            </option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-12">
                                    <div class="form-group">
                                        <label class="control-label">QTY</label>
                                        <input type="text" name="qty" class="form-control" placeholder=""
                                            value="<?php echo set_value('qty') ?>" required>
                                    </div>
                                </div>
                                <div class="col-12">
                                    <div class="form-group">
                                        <label class="control-label">Catatan</label>
                                        <input type="text" name="catatan" class="form-control" placeholder=""
                                            value="<?php echo set_value('catatan') ?>" required>
                                    </div>
                                </div>
                                <div class="col-12">
                                    <button class="btn btn-success btn-md btn-icon icon-left btn-block" name="submit"
                                        type="submit">
                                        <i class="fas fa-plus"></i> Tambah Transaksi
                                    </button>
                                </div>
                            </div>
                            <?php echo form_close(); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>