<!-- Main Content -->
<div class="main-content">
    <section class="section">
        <div class="section-header">
            <h1>Produk - AMP</h1>
            <div class="section-header-breadcrumb breadcrumb bg-success text-white-all">
                <div class="breadcrumb-item active"><a href="<?php echo base_url('admin/dashboard') ?>">Dashboard</a>
                </div>
                <div class="breadcrumb-item"><a href="<?php echo base_url('admin/produk') ?>">Produk</a></div>
                <div class="breadcrumb-item"><?php echo $title ?></div>
            </div>
        </div>
        <div class="section-body">
            <div class="row mb-1">
                <div class="col-12">
                    <?php 
                        // Notifikasi sukses
                        if ($this->session->flashdata('sukses')) {
                            echo '<p class="alert alert-success">';
                            echo $this->session->flashdata('sukses');
                        }elseif ($this->session->flashdata('error')) {
                            echo '<p class="alert alert-danger">';
                                echo $this->session->flashdata('error');
                        }
                    ?>
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <div class="card card-success">
                        <div class="card-header d-flex justify-content-between">
                            <h4>List Produk</h4>
                            <a href="<?php echo base_url('admin/produk/tambah') ?>"
                                class="btn btn-success btn-icon icon-left"><i class="fas fa-plus"></i> Tambah Data</a>
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table table-striped" id="table-2">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Gambar</th>
                                            <th>Nama Produk</th>
                                            <th>Jenis</th>
                                            <th>Merk</th>
                                            <th>Deskripsi Produk</th>
                                            <th>Harga Produk (RP)</th>
                                            <th>Margin Produk (RP)</th>
                                            <th>Ekspedisi (RP)</th>
                                            <th>Harga Pokok (RP)</th>
                                            <th>Harga Jual (RP)</th>
                                            <th>User</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php $no = 1; foreach ($produk as $produk) {?>
                                        <tr>
                                            <?php $harga_jual_lama = $produk->harga_pokok + $produk->margin_produk + $produk->harga_ekspedisi ?>
                                            <td><?php echo $no ?></td>
                                            <td>
                                                <img src="<?php echo base_url('assets/upload/image/thumbs/'.$produk->gambar) ?>"
                                                    class="img img-responsive" width="50">
                                            </td>
                                            <td><?php echo $produk->nama_produk ?></td>
                                            <td><?php echo $produk->nama_jenis ?></td>
                                            <td><?php echo $produk->nama_merk ?></td>
                                            <td><?php echo $produk->deskripsi_produk ?></td>
                                            <td><?php echo number_format($produk->harga_produk,'0', ',', '.') ?> </td>
                                            <td><?php echo number_format($produk->margin_produk,'0', ',', '.') ?></td>
                                            <td><?php echo $produk->kota_tujuan ?> - <?php echo number_format($produk->harga_ekspedisi,'0', ',', '.') ?></td>
                                            <td><?php echo number_format($produk->harga_pokok,'0', ',', '.') ?></td>
                                            <td><?php echo number_format($produk->harga_jual,'0', ',', '.') ?></td>
                                            <td><?php echo $produk->nama_user ?></td>
                                            <td>
                                                <a href="<?php echo base_url('admin/produk/edit/'.$produk->id_produk) ?>"
                                                    class="btn btn-warning btn-xs btn-icon icon-left my-2 my-lg-0 mr-2"><i
                                                        class="fa fa-edit"></i>
                                                    Edit</a>
                                                <?php include('delete.php') ?>
                                            </td>
                                        </tr>
                                        <?php $no++; }?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>