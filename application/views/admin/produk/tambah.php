<!-- Main Content -->
<div class="main-content">
    <section class="section">
        <div class="section-header">
            <h1>Data Produk</h1>
            <div class="section-header-breadcrumb breadcrumb bg-success text-white-all">
                <div class="breadcrumb-item active"><a href="<?php echo base_url('admin/dashboard') ?>">Dashboard</a>
                </div>
                <div class="breadcrumb-item"><a href="<?php echo base_url('admin/produk') ?>">Produk</a></div>
                <div class="breadcrumb-item"><?php echo $title ?></div>
            </div>
        </div>
        <div class="section-body">
            <div class="row">
                <div class="col-lg-12">
                    <div class="card card-success">
                        <div class="card-body">
                            <?php
                                //Error upload
                                if (isset($error)) {
                                echo '<div class="alert alert-warning">';
                                    echo $error;
                                    echo '</div>';
                                }
                                // Notifikasi error
                                echo validation_errors('<div class="alert alert-warning alert-dismissible show fade">','</div>');
                                // Form open
                                echo form_open_multipart(base_url('admin/produk/tambah'), 'class="form-horizontal"');
                            ?>
                            <div class="row d-flex">
                                <div class="col-12">
                                    <div class="form-group">
                                        <label class="control-label">Nama Produk</label>
                                        <input type="text" name="nama_produk" class="form-control" placeholder=""
                                            value="<?php echo set_value('nama_produk') ?>" required>
                                    </div>
                                </div>
                                <div class="col-12">
                                    <div class="form-group">
                                        <label class="control-label">Foto Produk</label>
                                        <div class="custom-file">
                                            <input type="file" name="gambar" class="custom-file-input" id="customFile">
                                            <label class="custom-file-label" for="customFile">Pilih Foto</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-12">
                                    <div class="form-group">
                                        <label class="control-label">Jenis</label>
                                        <select name="id_jenis" class="form-control">
                                            <?php foreach ($jenis as $jenis) {?>
                                            <option value="<?php echo $jenis->id_jenis ?>">
                                                <?php echo $jenis->nama_jenis ?>
                                            </option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-12">
                                    <div class="form-group">
                                        <label class="control-label">Merk</label>
                                        <select name="id_merk" class="form-control">
                                            <?php foreach ($merk as $merk) {?>
                                            <option value="<?php echo $merk->id_merk ?>">
                                                <?php echo $merk->nama_merk ?>
                                            </option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-12">
                                    <div class="form-group">
                                        <label class="control-label">Ekspedisi</label>
                                        <select name="id_ekspedisi" class="form-control">
                                            <?php foreach ($ekspedisi as $ekspedisi) {?>
                                            <option value="<?php echo $ekspedisi->id_ekspedisi ?>">
                                                <?php echo $ekspedisi->kota_tujuan ?> - <?php echo $ekspedisi->harga_ekspedisi ?>
                                            </option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-lg-6 col-12">
                                    <div class="form-group">
                                        <label class="control-label">Harga Pokok</label>
                                        <input type="text" name="harga_produk" class="form-control" placeholder=""
                                            value="<?php echo set_value('harga_produk') ?>" required>
                                    </div>
                                </div>
                                <div class="col-lg-6 col-12">
                                    <div class="form-group">
                                        <label class="control-label">Margin Produk</label>
                                        <input type="text" name="margin_produk" class="form-control" placeholder=""
                                            value="<?php echo set_value('margin_produk') ?>" required>
                                    </div>
                                </div>
                                <div class="col-12">
                                    <div class="form-group">
                                        <label class="control-label">Deskripsi Produk</label>
                                        <input type="text" name="deskripsi_produk" class="form-control" placeholder=""
                                            value="<?php echo set_value('deskripsi_produk') ?>" required>
                                    </div>
                                </div>
                                <div class="col-12">
                                    <button class="btn btn-success btn-md btn-icon icon-left btn-block" name="submit"
                                        type="submit">
                                        <i class="fas fa-plus"></i> Tambah Produk
                                    </button>
                                </div>
                            </div>
                            <?php echo form_close(); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>