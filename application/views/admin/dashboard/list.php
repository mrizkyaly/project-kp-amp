<!-- Main Content -->
<div class="main-content">
    <section class="section">
        <div class="section-header">
            <h1>Dashboard - AMP</h1>
        </div>

        <div class="section-body">
            <div class="row sortable-card">
                <div class="col-6 col-lg-4">
                    <div class="card card-success">
                        <div class="card-header">
                            <h4>Total User</h4>
                        </div>
                        <div class="card-body text-center">
                            <h3><?php echo $jumlah_user ?></h3>
                            <a href="<?php echo base_url('admin/user') ?>"
                                class="btn btn-outline-success btn-block">Lihat</a>
                        </div>
                    </div>
                </div>
                <div class="col-6 col-lg-4">
                    <div class="card card-primary">
                        <div class="card-header">
                            <h4>Total Ekspedisi</h4>
                        </div>
                        <div class="card-body text-center">
                            <h3><?php echo $jumlah_ekspedisi ?></h3>
                            <a href="<?php echo base_url('admin/ekspedisi')?>"
                                class="btn btn-outline-primary btn-block">Lihat</a>
                        </div>
                    </div>
                </div>
                <div class="col-6 col-lg-4">
                    <div class="card card-danger">
                        <div class="card-header">
                            <h4>Total Produk</h4>
                        </div>
                        <div class="card-body text-center">
                            <h3><?php echo $jumlah_produk ?></h3>
                            <a href="<?php echo base_url('admin/produk')?>"
                                class="btn btn-outline-danger btn-block">Lihat</a>
                        </div>
                    </div>
                </div>
                <div class="col-6 col-lg-4">
                    <div class="card card-warning">
                        <div class="card-header">
                            <h4>Total Transaksi</h4>
                        </div>
                        <div class="card-body text-center">
                            <h3><?php echo $jumlah_transaksi?></h3>
                            <a href="<?php echo base_url('admin/transaksi')?>"
                                class="btn btn-outline-warning btn-block">Lihat</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
</div>
</section>
</div>