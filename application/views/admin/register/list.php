<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no" name="viewport">
    <title><?php echo $title ?> - AMP</title>

    <!-- General CSS Files -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/admin/modules/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/admin/modules/fontawesome/css/all.min.css">

    <!-- CSS Libraries -->
    <link rel="stylesheet" href="assets/modules/bootstrap-social/bootstrap-social.css">

    <!-- Template CSS -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/admin/css/style.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/admin/css/components.css">
</head>

<body>
    <div id="app">
        <section class="section">
            <div class="container mt-5">
                <div class="row">
                    <div
                        class="col-12 col-sm-10 offset-sm-1 col-md-8 offset-md-2 col-lg-8 offset-lg-2 col-xl-8 offset-xl-2">
                        <div class="login-brand">
                            <img src="<?php echo base_url(); ?>assets/admin/img/logo-amp.png" alt="logo" width="100"
                                class="shadow-light rounded-circle">
                        </div>

                        <div class="card card-info">
                            <div class="card-header">
                                <h4>Register Admin</h4>
                            </div>

                            <div class="card-body">
                                <?php
                                    //Error upload
                                    if (isset($error)) {
                                    echo '<div class="alert alert-warning">';
                                        echo $error;
                                        echo '</div>';
                                    }
                                    // Notifikasi error
                                    echo validation_errors('<div class="alert alert-warning alert-dismissible show fade">','</div>');
                                    // Form open
                                    echo form_open_multipart(base_url('admin/register'), 'class="form-horizontal"');
                                ?>
                                <form method="POST">
                                    <div class="row">
                                        <div class="form-group col-12">
                                            <label for="nama_user">Nama User</label>
                                            <input id="nama_user" type="text" class="form-control" name="nama_user"
                                                autofocus>
                                        </div>
                                        <div class="form-group col-8">
                                            <label class="control-label">Foto User</label>
                                            <div class="custom-file">
                                                <input type="file" name="gambar" class="custom-file-input"
                                                    id="customFile">
                                                <label class="custom-file-label" for="customFile">Pilih Foto</label>
                                            </div>
                                        </div>
                                        <div class="form-group col-4">
                                            <label class="control-label">Jabatan</label>
                                            <select name="jabatan" class="form-control">
                                                <option value="admin">admin</option>
                                                <option value="pegawai">pegawai</option>
                                            </select>
                                        </div>

                                        <div class="form-group col-lg-4 col-12">
                                            <label for="nomor">Nomor</label>
                                            <input id="nomor" type="text" class="form-control" name="nomor" autofocus>
                                        </div>
                                        <div class="form-group col-lg-4 col-12">
                                            <label for="username">Username</label>
                                            <input id="username" type="text" class="form-control" name="username"
                                                autofocus>
                                        </div>
                                        <div class="form-group col-lg-4 col-12">
                                            <label for="password">Password</label>
                                            <input id="password" type="password" class="form-control" name="password"
                                                autofocus>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <button type="submit" class="btn btn-success btn-lg btn-block">
                                            Register
                                        </button>
                                    </div>
                                </form>
                                <?php echo form_close(); ?>
                            </div>
                        </div>
                        <!-- <div class="mt-5 text-muted text-center">
                            Already have an account? <a href="<?php base_url(); ?>root">Login Now</a>
                        </div> -->
                        <div class="simple-footer">
                            Copyright &copy; AMP 2021
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>

    <!-- General JS Scripts -->
    <script src="<?php echo base_url(); ?>assets/admin/modules/jquery.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/admin/modules/popper.js"></script>
    <script src="<?php echo base_url(); ?>assets/admin/modules/tooltip.js"></script>
    <script src="<?php echo base_url(); ?>assets/admin/modules/bootstrap/js/bootstrap.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/admin/modules/nicescroll/jquery.nicescroll.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/admin/modules/moment.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/admin/js/stisla.js"></script>

    <!-- JS Libraies -->

    <!-- Page Specific JS File -->

    <!-- Template JS File -->
    <script src="<?php echo base_url(); ?>assets/admin/js/scripts.js"></script>
    <script src="<?php echo base_url(); ?>assets/admin/js/custom.js"></script>
</body>

</html>