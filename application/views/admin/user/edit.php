<!-- Main Content -->
<div class="main-content">
    <section class="section">
        <div class="section-header">
            <h1>Data Pegawai</h1>
            <div class="section-header-breadcrumb breadcrumb bg-success text-white-all">
                <div class="breadcrumb-item active"><a href="<?php echo base_url('admin/dashboard') ?>">Dashboard</a>
                </div>
                <div class="breadcrumb-item"><a href="<?php echo base_url('admin/user') ?>">Pegawai</a></div>
                <div class="breadcrumb-item"><?php echo $title ?></div>
            </div>
        </div>
        <div class="section-body">
            <div class="row">
                <div class="col-lg-12">
                    <div class="card card-success">
                        <div class="card-body">
                            <?php
                        //Error upload
                        if (isset($error)) {
                        echo '<div class="alert alert-warning">';
                            echo $error;
                            echo '</div>';
                        }
                        // Notifikasi error
                        echo validation_errors('<div class="alert alert-warning alert-dismissible show fade">','</div>');
                        // Form open
                        echo form_open_multipart(base_url('admin/user/edit/'.$user->id_user), 'class="form-horizontal"');
                    ?>
                            <div class="row d-flex">
                                <div class="col-12">
                                    <div class="form-group">
                                        <label class="control-label">Nama Pegawai</label>
                                        <input type="text" name="nama_user" class="form-control" placeholder=""
                                            value="<?php echo $user->nama_user ?>" required>
                                    </div>
                                </div>
                                <div class="col-lg-8 col-12">
                                    <div class="form-group">
                                        <label class="control-label">Foto Pegawai</label>
                                        <div class="custom-file">
                                            <input type="file" name="gambar" class="custom-file-input" id="customFile">
                                            <label class="custom-file-label" for="customFile">Pilih Foto</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-12">
                                    <div class="form-group">
                                        <label class="control-label">Jabatan</label>
                                        <select name="jabatan" class="form-control">
                                            <option value="admin">admin</option>
                                            <option value="pegawai">pegawai</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-12">
                                    <div class="form-group">
                                        <label class="control-label">Nomor</label>
                                        <input type="text" name="nomor" class="form-control" placeholder=""
                                            value="<?php echo $user->nomor ?>" required>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-12">
                                    <div class="form-group">
                                        <label class="control-label">Username</label>
                                        <input type="text" name="username" class="form-control" placeholder=""
                                            value="<?php echo $user->username ?>" required>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-12">
                                    <div class="form-group">
                                        <label class="control-label">Password</label>
                                        <input type="password" name="password" class="form-control" placeholder=""
                                            value="<?php echo $user->password ?>" required>
                                    </div>
                                </div>
                                <div class="col-12">
                                    <button class="btn btn-success btn-md btn-icon icon-left btn-block" name="submit"
                                        type="submit">
                                        <i class="fas fa-plus"></i> Tambah Pegawai
                                    </button>
                                </div>
                            </div>
                            <?php echo form_close(); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>