<!-- Main Content -->
<div class="main-content">
    <section class="section">
        <div class="section-header">
            <h1>satuan - AMP</h1>
            <div class="section-header-breadcrumb breadcrumb bg-success text-white-all">
                <div class="breadcrumb-item active"><a href="<?php echo base_url('admin/dashboard') ?>">Dashboard</a>
                </div>
                <div class="breadcrumb-item"><a href="<?php echo base_url('admin/satuan') ?>">satuan</a></div>
                <div class="breadcrumb-item"><?php echo $title ?></div>
            </div>
        </div>
        <div class="section-body">
            <div class="row mb-1">
                <div class="col-12">
                    <?php 
                        // Notifikasi sukses
                        if ($this->session->flashdata('sukses')) {
                            echo '<p class="alert alert-success">';
                            echo $this->session->flashdata('sukses');
                        }elseif ($this->session->flashdata('error')) {
                            echo '<p class="alert alert-danger">';
                                echo $this->session->flashdata('error');
                        }
                    ?>
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <div class="card card-success">
                        <div class="card-header d-flex justify-content-between">
                            <h4>List satuan</h4>
                            <a href="<?php echo base_url('admin/satuan/tambah') ?>"
                                class="btn btn-success btn-icon icon-left"><i class="fas fa-plus"></i> Tambah Data</a>
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table table-striped" id="table-2">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Nama</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php $no = 1; foreach ($satuan as $satuan) {?>
                                        <tr>
                                            <td><?php echo $no ?></td>
                                            <td><?php echo $satuan->nama_satuan ?></td>
                                            <td>
                                                <a href="<?php echo base_url('admin/satuan/edit/'.$satuan->id_satuan) ?>"
                                                    class="btn btn-warning btn-xs btn-icon icon-left my-2 my-lg-0 mr-2"><i
                                                        class="fa fa-edit"></i>
                                                    Edit</a>
                                                <?php include('delete.php') ?>
                                            </td>
                                        </tr>
                                        <?php $no++; }?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>