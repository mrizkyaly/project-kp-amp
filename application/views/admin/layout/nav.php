<div class="main-sidebar">
    <aside id="sidebar-wrapper">
        <div class="d-flex justify-content-center my-1">
            <img src="<?php echo base_url(); ?>assets/admin/img/logo-amp.png" alt="logo" class="rounded-circle w-50">
        </div>
        <div class="sidebar-brand">
            <a href="<?php echo base_url('admin/dashboard'); ?>">Admin AMP</a>
        </div>
        <div class="sidebar-brand sidebar-brand-sm">
            <a href="<?php echo base_url('admin/dashboard'); ?>">AMP</a>
        </div>
        <ul class="sidebar-menu">
            <li class="menu-header">Dashboard</li>
            <li>
                <a class="nav-link" href="<?php echo base_url('admin/dashboard'); ?>"><i
                        class="fas fa-tachometer-alt"></i> <span>Dasboard</span></a>
            </li>
            <li class="menu-header">Produk AMP</li>
            <li class="nav-item dropdown">
                <a href="#" class="nav-link has-dropdown"><i class="fas fa-archive"></i><span>Data Produk</span></a>
                <ul class="dropdown-menu">
                    <li><a class="nav-link" href="<?php echo base_url('admin/produk'); ?>">Daftar Produk</a></li>
                    <li><a class="nav-link" href="<?php echo base_url('admin/jenis'); ?>">Jenis Produk</a></li>
                    <li><a class="nav-link" href="<?php echo base_url('admin/merk'); ?>">Merk Produk</a></li>
                    <li><a class="nav-link" href="<?php echo base_url('admin/satuan'); ?>">Satuan Produk</a></li>
                </ul>
            </li>
            <li>
                <a class="nav-link" href="<?php echo base_url('admin/ekspedisi'); ?>"><i
                        class="fas fa-truck-moving"></i>
                    <span>Ekspedisi</span></a>
            </li>
            <li class="menu-header">Transaksi AMP</li>
            <li>
                <a class="nav-link" href="<?php echo base_url('admin/transaksi'); ?>"><i
                        class="fas fa-shopping-cart"></i>
                    <span>Transaksi</span>
                </a>
            </li>
            <li class="menu-header">Pegawai AMP</li>
            <li>
                <a class="nav-link" href="<?php echo base_url('admin/user'); ?>"><i class="fas fa-user-friends"></i>
                    <span>Pegawai</span></a>
            </li>
        </ul>
        <div class="mt-4 mb-4 p-3 hide-sidebar-mini">
            <a href="<?php echo base_url() ?>" class="btn btn-success btn-lg btn-block btn-icon-split">
                <i class="fas fa-rocket"></i> Laman Pelanggan
            </a>
        </div>
    </aside>
</div>