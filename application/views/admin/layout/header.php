<div class="navbar-bg bg-info"></div>
<nav class="navbar navbar-expand-lg main-navbar justify-content-between">
    <ul class="navbar-nav">
        <li>
            <a href="#" data-toggle="sidebar" class="nav-link nav-link-lg"><i class="fas fa-bars"></i></a>
        </li>
    </ul>
    <ul class="navbar-nav navbar-right float-right">
        <li class="dropdown"><a href="#" data-toggle="dropdown"
                class="nav-link dropdown-toggle nav-link-lg nav-link-user">
                <?php $gambar = $this->session->userdata('gambar');?>
                <img alt="image" src="<?php echo base_url('assets/upload/image/thumbs/'.$gambar) ?>"
                    class="rounded-circle mr-1">
                <div class="d-sm-none d-lg-inline-block">Hi, <?php echo $this->session->userdata('nama_user'); ?></div>
            </a>
            <div class="dropdown-menu dropdown-menu-right">
                <?php $id_user = $this->session->userdata('id_user');?>
                <div class="dropdown-title">Logged in <?php echo date('d M Y') ?></div>
                <a href="<?php echo base_url('admin/user/edit/'.$id_user) ?>" class="dropdown-item has-icon">
                    <i class="far fa-user"></i> Profile
                </a>
                <div class="dropdown-divider"></div>
                <a href="<?php echo base_url('root/logout')?>" class="dropdown-item has-icon text-danger">
                    <i class="fas fa-sign-out-alt"></i> Logout
                </a>
            </div>
        </li>
    </ul>
</nav>