<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no" name="viewport">
    <title><?php echo $title ?> - AMP</title>

    <!-- General CSS Files -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/admin/modules/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/admin/modules/fontawesome/css/all.min.css">

    <!-- CSS Libraries -->
    <link rel="stylesheet" href="assets/modules/bootstrap-social/bootstrap-social.css">

    <!-- Template CSS -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/admin/css/style.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/admin/css/components.css">
</head>

<body>
    <div id="app">
        <section class="section">
            <div class="container mt-5">
                <div class="row">
                    <div
                        class="col-12 col-sm-8 offset-sm-2 col-md-6 offset-md-3 col-lg-6 offset-lg-3 col-xl-4 offset-xl-4">
                        <div class="login-brand">
                            <img src="<?php echo base_url(); ?>assets/admin/img/logo-amp.png" alt="logo" width="100"
                                class="shadow-light rounded-circle">
                        </div>

                        <div class="card card-info">
                            <div class="card-header">
                                <h4>Login Admin</h4>
                            </div>

                            <div class="card-body">
                                <div class="d-block">
                                    <?php
                                        // Motifikasi eror
                                        echo validation_errors('<div class="alert alert-warning">','</div>');

                                        
                                        // Notifikasi gagal login
                                        if ($this->session->flashdata('warning')) {
                                            echo '<div class="alert alert-danger alert-dismissible">';
                                            echo $this->session->flashdata('warning');
                                            echo '</div';
                                        }
                                        // Notifikasi logout
                                        if ($this->session->flashdata('sukses')) {
                                            echo '<div class="alert alert-success">';
                                            echo $this->session->flashdata('sukses');
                                            echo '</div';
                                        }
                                        
                                        // form open login
                                        echo form_open(base_url('root'));

                                    ?>
                                </div>
                                <form method="POST">
                                    <div class="form-group">
                                        <label for="username">Username</label>
                                        <input id="username" type="username" class="form-control" name="username"
                                            tabindex="1" required autofocus>
                                        <div class="invalid-feedback">
                                            Please fill in your username
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="d-block">
                                            <label for="password" class="control-label">Password</label>
                                        </div>
                                        <input id="password" type="password" class="form-control" name="password"
                                            tabindex="2" required>
                                        <div class="invalid-feedback">
                                            please fill in your password
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <button type="submit" class="btn btn-success btn-lg btn-block" tabindex="4">
                                            Login
                                        </button>
                                    </div>
                                </form>
                                <?php echo form_close(); ?>
                            </div>
                        </div>
                        <div class="mt-5 text-muted text-center">
                            Don't have an account? <a href="<?php base_url(); ?>admin/register">Create One</a>
                        </div>
                        <div class="simple-footer">
                            Copyright &copy; AMP 2021
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>

    <!-- General JS Scripts -->
    <script src="<?php echo base_url(); ?>assets/admin/modules/jquery.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/admin/modules/popper.js"></script>
    <script src="<?php echo base_url(); ?>assets/admin/modules/tooltip.js"></script>
    <script src="<?php echo base_url(); ?>assets/admin/modules/bootstrap/js/bootstrap.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/admin/modules/nicescroll/jquery.nicescroll.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/admin/modules/moment.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/admin/js/stisla.js"></script>

    <!-- JS Libraies -->

    <!-- Page Specific JS File -->

    <!-- Template JS File -->
    <script src="<?php echo base_url(); ?>assets/admin/js/scripts.js"></script>
    <script src="<?php echo base_url(); ?>assets/admin/js/custom.js"></script>
</body>

</html>