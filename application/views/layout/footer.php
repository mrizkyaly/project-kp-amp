<!-- Footer -->
<div class="footer">
    <div class="container ">
        <div class="container-fluid text-md-left pt-5">
            <div class="row footer-details text-center">
                <div class="col-lg-6 py-2 about-us ">
                    <h5>Tentang Kita</h5>
                    <p>Artha Mulia Pamenang merupakan Distributor Dan Supplier material bahan bangunan seperti Panel
                        Lantai, Bata Ringan dan Semen Mortar. Kami juga melayani Renovasi dan Pembangunan Rumah serta
                        Jasa Survei dan Pemetaan.</p>
                    <i class="fa fa-facebook" aria-hidden="true"></i>
                    <i class="fa fa-youtube-play" aria-hidden="true"></i>
                    <i class="fa fa-twitter" aria-hidden="true"></i>
                    <i class="fa fa-instagram" aria-hidden="true"></i>
                </div>
                <div class="col-lg-6 py-2 company ">
                    <h5>Perusahaan</h5>
                    <ul class="company-item">
                        <li>
                            <a href="https://jualbataringanmurah.com/">
                                Official Website
                            </a>
                        </li>
                        <li>
                            <a href="https://jualbataringanmurah.com/">
                                Kontak
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
<div class="footer-copyright text-left py-3">
    <div class="container text-center">
        <p>2020 &#169; Artha Mulia Pamenang</p>
    </div>
</div>
<!-- End Footer -->

<!-- jQuery CDN - Slim version (=without AJAX) -->
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
    integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous">
</script>
<!-- Popper.JS -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js"
    integrity="sha384-cs/chFZiN24E4KMATLdqdvsezGxaGsi4hLGOzlXwp5UZB1LY//20VyM2taTB4QvJ" crossorigin="anonymous">
</script>
<!-- Bootstrap JS -->
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js"
    integrity="sha384-uefMccjFJAIv6A+rW+L4AHf99KvxDjWSu1z9VI8SKNVmz4sk7buKt/6v9KI65qnm" crossorigin="anonymous">
</script>
<!-- jQuery Custom Scroller CDN -->
<script
    src="https://cdnjs.cloudflare.com/ajax/libs/malihu-custom-scrollbar-plugin/3.1.5/jquery.mCustomScrollbar.concat.min.js">
</script>

<script src="<?php echo base_url() ?>assets/client/assets/js/main.js" type="text/javascript"></script>
<script src="<?php echo base_url() ?>assets/client/assets/js/scroll.js" type="text/javascript"></script>
</body>

</html>