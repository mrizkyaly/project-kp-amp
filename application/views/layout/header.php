<body>
    <!-- Header -->
    <section class="header mx-sm-4">
        <div class="wrapper">
            <!-- Sidebar  -->
            <nav id="sidebar1" class="sidebar1">
                <div id="dismiss1">
                    <i class="fas fa-times"></i>
                </div>
                <div class="sidebar-header1">
                    <h3>Menu</h3>
                </div>
                <ul class="list-unstyled components">
                    <li>
                        <a href="<?php echo base_url('beranda') ?>">Beranda</a>
                    </li>
                    <li>
                        <a href="<?php echo base_url('produk')?>">Produk</a>
                    </li>
                </ul>
            </nav>
            <!-- Page Content  -->
        </div>
        <div class="overlay"></div>
        <nav class="navbar navbar-expand-lg fixed-top">
            <div class="container d-flex justify-content-beetween">
                <button class="btn d-lg-none btn-icon-nav shadow-none" id="sidebarCollapse1" type="button">
                    <i class="fas fa-align-justify"></i>
                </button>
                <div class="collapse navbar-collapse">
                    <ul class="navbar-nav">
                        <li class="nav-item <?= $this->uri->segment('1') == '' ? 'active' : '' ?>">
                            <a class="nav-link" href="<?php echo base_url('')?>">Beranda</a>
                        </li>
                        <li class="nav-item <?= $this->uri->segment('1') == 'produk' ? 'active' : '' ?>">
                            <a class="nav-link" href="<?php echo base_url('produk')?>">Produk</a>
                        </li>
                    </ul>
                </div>
                <a class="navbar-brand text-center" href="<?php echo base_url('')?>">
                    <img src="<?php echo base_url(); ?>assets/admin/img/logo-amp.png" class="img-fluid" 
                        alt="" style="width: 50px;">
                </a>
            </div>
        </nav>
    </section>
    <!-- End header -->