<!-- Our Product -->
<section class="my-produk m-5">
    <div class="container-fluid py-5 px-4">
        <div class="row d-flex justify-content-center">
            <div class="col-8 p-3 align-items-center">
                <?php 
                        // Notifikasi sukses
                        if ($this->session->flashdata('sukses')) {
                            echo '<p class="alert alert-success">';
                            echo $this->session->flashdata('sukses');
                        }elseif ($this->session->flashdata('error')) {
                            echo '<p class="alert alert-danger">';
                                echo $this->session->flashdata('error');
                        }
                    ?>
            </div>
        </div>
        <h2 class="judul pb-3">Semua Produk</h2>
        <div class="mx-auto heading-line"></div>
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb mt-5">
                <li class="breadcrumb-item"><a href="<?php echo base_url()?>">Beranda</a></li>
                <li class="breadcrumb-item"><a href="<?php echo base_url('produk')?>">Produk</a></li>
                <li class="breadcrumb-item active" aria-current="page">Semua Produk</li>
            </ol>
        </nav>

        <div class="row py-3 px-2">
            <?php $no=1; foreach ($produk as $produk) { ?>
            <div class="col-md-3 py-1">
                <div class="card produk">
                    <img src="<?php echo base_url('assets/upload/image/'.$produk->gambar) ?>" class="card-img-top"
                        alt="...">
                    <div class="card-body">
                        <h5 class="card-title"><?php echo $produk->nama_produk ?></h5>
                        <p class="card-text">Rp.<?php echo number_format($produk->harga_jual,'0',',','.') ?>,-</p>
                        <a href="<?php echo base_url('produk/detail/'.$produk->slug_produk)?>"
                            class="btn btn-primary btn-produk">Lihat Produk</a>
                    </div>
                </div>
            </div>
            <?php $no++; }?>
        </div>
    </div>
</section>
<!-- End Our Product -->