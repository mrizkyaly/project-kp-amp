<!-- Transaksi -->
<div class="transaksi">
    <div class="container-fluid py-1 px-5">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb mt-5">
                <li class="breadcrumb-item"><a href="<?php echo base_url()?>">Beranda</a></li>
                <li class="breadcrumb-item active" aria-current="page">Transaksi</li>
            </ol>
        </nav>
        <div class="row">
            <div class="col-lg-5">
                <div class="container py-4 produk-selected">
                    <div class="card py-2 w-100">
                        <div class="card-body p-0">
                            <h4 class="card-title">Produk Terpilih</h4>
                            <hr>
                            <div class="card py-0">
                                <div class=" card-body">
                                    <div class="row p-0">
                                        <div class="col-md-4 text-center p-1">
                                            <img src="<?php echo base_url('assets/upload/image/'.$produk->gambar) ?>"
                                                class="img-fluid" alt="...">
                                        </div>
                                        <div class="col-md-8 px-2 py-4">
                                            <h6><?php echo $produk->nama_produk ?></h6>
                                            <hr>
                                            <div class="d-flex justify-content-between">
                                                <p>Rp.<?php echo number_format($produk->harga_jual,'0',',','.') ?></p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-7">
                <div class="container py-4 shipping">
                    <div class="card w-100">
                        <article class="card-body p-5">
                            <h4 class="card-title">Detail Penjualan</h4>
                            <hr>
                            <?php 
                                echo validation_errors('<div class="alert alert-warning ">','</div>');
                                
                                if ($this->session->flashdata('sukses')) {
                                    echo '<p class="alert alert-success">';
                                    echo $this->session->flashdata('sukses');
                                }elseif ($this->session->flashdata('error')) {
                                    echo '<p class="alert alert-danger">';
                                        echo $this->session->flashdata('error');
                                }
                            ?>
                            <form class="form py-2" method="POST" action="<?php echo base_url('produk/transaksi/'.$produk->id_produk); ?>">
                                <div class="form-group">
                                    <label class="control-label">Nama Pelanggan</label>
                                    <input name="nama_pelanggan" class="form-control shadow-none"
                                        value="<?php echo set_value('nama_pelanggan') ?>" 
                                        type="text">
                                </div>
                                <div class="form-group">
                                    <label class="control-label">Alamat Pelanggan</label>
                                    <input name="alamat_pelanggan" class="form-control shadow-none"
                                        
                                        value="<?php echo set_value('alamat_pelanggan') ?>" type="text">
                                </div>
                                <div class="form-group">
                                    <label class="control-label">Nomor Telepon</label>
                                    <input name="telp_pelanggan" class="form-control shadow-none" 
                                        value="<?php echo set_value('telp_pelanggan') ?>" type="text">
                                </div>
                                <div class="form-group">
                                    <label class="control-label">Kuantitas Produk</label>
                                    <input name="qty" class="form-control shadow-none" 
                                        value="<?php echo set_value('qty') ?>" type="text">
                                </div>
                                <div class="form-group">
                                    <label class="control-label">Satuan Produk</label>
                                    <select name="id_satuan" class="form-control shadow-none radius-0">
                                        <?php foreach ($satuan as $satuan) {?>
                                        <option value="<?php echo $satuan->id_satuan ?>">
                                            <?php echo $satuan->nama_satuan ?>
                                        </option>
                                        <?php } ?>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <div class="g-recaptcha" data-sitekey="<?php echo $this->config->item('google_key') ?>"></div> 
                                </div>
                                <div class="d-flex justify-content-between">
                                    <button type="submit" class="btn btn-beli p-2">Beli Sekarang</button>
                                </div>
                            </form>
                        </article>
                    </div>
                </div>
                <div class="container py-4 shipping">
                    <div class="card w-100">
                        <div class="card-body p-5">
                            <h4 class="card-title">Tata Cara Pembayaran</h4>
                            <hr>
                            <ol>
                                <li>Bayarkan Kepada Nomer Rekening :xxxxxxxxx sejumlah banyak transaksi</li>
                                <li>Admin Akan Mengkonfirmasi ke Nomer WA yang telah didaftarkan</li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- end transaksi -->
<?php //echo form_close(); ?>