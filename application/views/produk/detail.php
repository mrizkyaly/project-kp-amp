<!-- Details Produk -->
<div class="detail-produk">
    <div class="container-fluid p-5">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="<?php echo base_url()?>">Beranda</a></li>
                <li class="breadcrumb-item"><a href="<?php echo base_url('produk')?>">Produk</a></li>
                <li class="breadcrumb-item active" aria-current="page"><?php echo $produk->nama_produk ?></li>
            </ol>
        </nav>
        <div class="row ">
            <div class="col-md-7 py-2">
                <img src="<?php echo base_url('assets/upload/image/'.$produk->gambar) ?>" class="img-fluid w-100"
                    alt="Responsive image">
            </div>
            <div class="col-md-5 py-2">
                <div class="container mx-1">
                    <h6><?php echo $produk->nama_merk ?> - <?php echo $produk->nama_jenis ?></h6>
                    <h3><?php echo $produk->nama_produk ?></h3>
                    <h5>Rp.<?php echo number_format($produk->harga_jual,'0',',','.') ?>,-</h5>
                    <hr>
                    <a href="transaksi.html">
                        <a href="<?php echo base_url('produk/transaksi/'.$produk->id_produk)?>" type="button"
                            class="btn btn-detail-produk mt-4 w-100 shadow-none">Beli Sekarang</a>
                    </a>
                    <div class="card mt-3">
                        <div class="card-body">
                            <h6>Deskripsi Produk</h6>
                            <hr>
                            <p><?php echo $produk->deskripsi_produk ?>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- End Details Produk -->