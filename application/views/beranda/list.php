<!-- Slider Menu -->
<section class="slider-menu">
    <div id="carouselExampleIndicators" class="carousel slide carousel-fade" data-ride="carousel">
        <div class="carousel-inner">
            <div class="carousel-item active">
                <img class="d-block w-100 img-fluid" style="object-fit: cover;"
                    src="<?php echo base_url() ?>assets/client/assets/img/banner/slide2.jpg" alt="First slide">
                <div class="carousel-caption mb-5">
                    <h5>Distributor Bata Ringan dan Panel Lantai #1 di Jawa Timur</h5>
                    <h2>Amartha Mulia Pamenang</h2>
                    <a href="<?php echo base_url('produk')?>" class="btn btn-slider ">Belanja Sekarang</a>
                </div>
            </div>
            <div class="carousel-item">
                <img class="d-block w-100 img-fluid" style="object-fit: cover;"
                    src="<?php echo base_url() ?>assets/client/assets/img/banner/slide3.png" alt="Second slide">
                <div class="carousel-caption mb-5">
                    <h5>Distributor Bata Ringan dan Panel Lantai #1 di Jawa Timur</h5>
                    <h2>Amartha Mulia Pamenang</h2>
                    <a href="<?php echo base_url('produk')?>" class="btn btn-slider">Belanja Sekarang</a>
                </div>
            </div>
            <div class="carousel-item">
                <img class="d-block w-100 img-fluid" style="object-fit: cover;"
                    src="<?php echo base_url() ?>assets/client/assets/img/banner/slide6.jpg" alt="Third slide">
                <div class="carousel-caption mb-5">
                    <h5>Distributor Bata Ringan dan Panel Lantai #1 di Jawa Timur</h5>
                    <h2>Amartha Mulia Pamenang</h2>
                    <a href="<?php echo base_url('produk')?>" class="btn btn-slider">Belanja Sekarang</a>
                </div>
            </div>
        </div>
        <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
        </a>
        <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
        </a>
    </div>
</section>
<!-- End Slider -->

<!-- Our Product -->
<section class="favorite-produk">
    <div class="container py-5">
        <h2 class="py-3 judul">Produk AMP</h2>
        <div class="mx-auto heading-line"></div>
        <div class="row py-5 px-3">
            <?php $no=1; foreach ($produk as $produk) { ?>
            <div class="col-6 col-md-3 py-1">
                <div class="card produk">
                    <img src="<?php echo base_url('assets/upload/image/'.$produk->gambar) ?>" class="card-img-top"
                        alt="...">
                    <div class="card-body">
                        <h5 class="card-title"><?php echo $produk->nama_produk ?></h5>
                        <p class="card-text">Rp.<?php echo number_format($produk->harga_jual,'0',',','.') ?>,-</p>
                        <a href="<?php echo base_url('produk/detail/'.$produk->slug_produk)?>"
                            class="btn btn-primary btn-produk">Lihat Produk</a>
                    </div>
                </div>
            </div>
            <?php $no++; }?>
        </div>
        <div class="d-flex justify-content-center">
            <a href="<?php echo base_url('produk')?>" class="btn btn-lg btn-product">Lihat Semua</a>
        </div>
    </div>
</section>
<!-- End Our Product -->